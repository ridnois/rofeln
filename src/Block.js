const SHA256 = require('crypto-js/sha256');

class Block {
    constructor(timestamp, transactions, previousHash = '') {
        this.timestamp = timestamp;
        this.transactions = transactions;
        this.previousHash = previousHash;
        this.hash = this.calculateHash();
        this.nonce = 0;
    }
    calculateHash() {
        return SHA256(this.index + this.previousHash + this.timestamp +this.nonce + JSON.stringify(this.transactions)).toString();
    }
    mineBlock(difficulty){
        while(this.hash.substr(0,difficulty) !== Array(difficulty +1).join("0")){
            this.nonce +=1;
            this. hash = this.calculateHash();
        } 
        // console.log(`Mined on ${this.nonce} attempts`);
    }
    hasValidTransactions(){
        for(const tx of this.transactions){
            if(!tx.isValid()){
                return false;
            }
        }
        return true;
    }
}

module.exports = Block;