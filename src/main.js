const Blockchain = require('./Blockchain');
const Transaction = require('./Transaction');
const EC = require('elliptic').ec;
const ec =  new EC('secp256k1');

const myKey = ec.keyFromPrivate('608ed27d15f0fa6fec6481f7095034e7c8e34763de3dff72cccaa93f496e3573');
const myWalletAddress = myKey.getPublic('hex');

const loudCoin = new Blockchain();
const txt1 = new Transaction(myWalletAddress, 'Key goes here', -500);
txt1.signTransaction(myKey);
loudCoin.addTransaction(txt1);
loudCoin.minePendingTransactions(myWalletAddress);
loudCoin.getBalanceOfAdress(myWalletAddress)
console.log(`Is chain valid?: ${loudCoin.isChainValid()}`)