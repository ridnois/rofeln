const Block = require('./Block');
const Transaction = require('./Transaction');

class Blockchain {
    
    constructor() {
        this.chain = [this.createGenesisBlock()];
        this.difficulty = 4;
        this.pendingTransactions = [];
        this.miningReward = 100;
    }
    
    createGenesisBlock() {
        return new Block(0, Date.now(), "Genesis block");
    }
    
    getLatestBlock() {
        return this.chain[this.chain.length - 1];
    }
    
    minePendingTransactions(miningRewardAddress) {

        let block = new Block(Date.now(), this.pendingTransactions);
        block.previousHash = this.getLatestBlock().hash;
        block.mineBlock(this.difficulty);
        this.chain.push(block);
        console.log("Block succesfully mined!")
        this.pendingTransactions = [
            new Transaction(null, miningRewardAddress, this.miningReward)
        ]
    }
    
    addTransaction(transaction){
        if(!transaction.fromAddress || !transaction.toAddress){
            throw new Error("Transaction must include 'from' and 'to' address");
        }
        if(!transaction.isValid()){
            throw new Error("Cannot add invalid transaction to chain");
        }
        this.pendingTransactions.push(transaction);
    }

    getBalanceOfAdress(address){
        var balance = 0;
        for(const block of this.chain){
            // console.log(block.transactions);
            for(const trans in block.transactions){
                if(block.transactions[trans].fromAddress === address){
                    balance -= block.transactions[trans].mount;
                }
                if(block.transactions[trans].toAddress === address){
                    balance += block.transactions[trans].mount;
                }
            }
        }
        return balance;
    }

    isChainValid() {
        for (let i = 1; i < this.chain.length; i++) {
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];
            if(!currentBlock.hasValidTransactions()){
                return false;
            }
            if (currentBlock.hash !== currentBlock.calculateHash()) {
                return false;
            }
            if (currentBlock.previousHash !== previousBlock.hash) {
                return false;
            }
        }
        return true;
    }


}

module.exports = Blockchain;


