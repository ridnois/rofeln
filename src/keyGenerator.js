const EC = require('elliptic').ec;
const ec =  new EC('secp256k1');

const key  = ec.genKeyPair();
const publicKey = key.getPublic('hex');
const privateKey = key.getPrivate('hex');

console.log('Public: \n',publicKey,'\n Private:\n' ,privateKey);
//Private 
// 608ed27d15f0fa6fec6481f7095034e7c8e34763de3dff72cccaa93f496e3573
//Public
//04af12e87f7d2e052b997831e10f3efabfc3ad5f7c3e2c1ef1066733e9f3d5b582e2c32495dad268a0b60d65ed1b6e51391d82da06c90879b8ef9ca8aab4aeeb25